# 0.2.0-alpha
- (Breaking) added data field to Node.
- Update now_playing position in player_update
- Add requester field to TrackQueue
- Destroy now skips if possible.

# 0.1.4-alpha
- UserId parameters take Into trait.
- Added some methods to GuildId.

# 0.1.3-alpha
- Added has to GuildID

# 0.1.2-alpha
- added equalization support.

# 0.1.1-alpha
- Pushed to crates.io

# 0.1.0-alpha
- Rewrote the library.
- Added events.
- Optimized the codebase.
- Removed all the clones from the examples.
- Remade easy queues.

# 0.0.3-alpha
- Added easy queues
- Added nodes

# 0.0.2-alpha
- Added start time to play()
- Added finish time to play()
- Added overwrite current stream to play()
- Added pause()
- Added resume()
- Added stop()
- Added destroy()
- Added seek()
- Added set_volume()
- Updated serenity.

# 0.0.1-alpha
- Initial release
